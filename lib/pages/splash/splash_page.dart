import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spacebook/blocs/search/search_bloc.dart';
import 'package:spacebook/connectors/nasa_connector.dart';

import '../../theme.dart';
import '../discover/discover_page.dart';

class SplashPage extends StatefulWidget {
  static const name = '/';

  NasaConnector nasaConnector;

  SplashPage({Key? key, required this.nasaConnector}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    // Redirection lorsque les données sont chargées
    context.read<SearchCubit>().search('');
    context.watch<SearchCubit>().stream.forEach((element) => {
          if (element.status == SearchStatus.success)
            {
              Navigator.pushNamedAndRemoveUntil(
                  context, DiscoverPage.name, (route) => false)
            }
        });

    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [ThemeApp.kBlue, ThemeApp.kDeepBlue]),
        ),
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Transform.scale(
                    scale: 1.3,
                    child: Image.asset('assets/images/splash_line.png')),
                const Center(
                  child: Text('SPACE\nBOOK',
                      style: TextStyle(
                          fontSize: 80,
                          fontFamily: 'PermanentMarker',
                          color: Colors.white,
                          height: 0.65,
                          letterSpacing: 1.2,
                          shadows: [
                            Shadow(
                                color: ThemeApp.kLightBlue,
                                blurRadius: 1,
                                offset: Offset(-5, 3))
                          ])),
                )
              ],
            ),
            Container(
              height: 70,
            ),
            const CircularProgressIndicator(
              strokeWidth: 1.0,
              color: Colors.white,
            )
          ],
        )),
      ),
    );
  }
}
