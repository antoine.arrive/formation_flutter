import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:more/more.dart' show ChunkedExtension;
import 'package:spacebook/pages/discover/components/mosaic_block.dart';
import 'package:spacebook/pages/discover/components/search_bar.dart';

import '../../blocs/search/search_bloc.dart';

class DiscoverPage extends StatefulWidget {
  static const name = '/discover';
  const DiscoverPage({Key? key}) : super(key: key);

  @override
  State<DiscoverPage> createState() => _DiscoverPageState();
}

class _DiscoverPageState extends State<DiscoverPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("SPACEBOOK"),
        centerTitle: true,
      ),
      body: Column(children: [
        const SearchBar(),
        BlocBuilder<SearchCubit, Search>(
          builder: (context, search) => (search.status == SearchStatus.pending)
              ? const Padding(
                  padding: EdgeInsets.only(top: 50),
                  child: CircularProgressIndicator(
                    strokeWidth: 1.0,
                    color: Colors.white,
                  ),
                )
              : Expanded(
                  child: ListView.builder(
                    itemCount: search.medias.length,
                    itemBuilder: (context, index) {
                      return MosaicBlock(
                        medias: search.medias.chunked(3).toList()[index],
                        reverse: (index % 2) != 0,
                      );
                    },
                  ),
                ),
        ),
      ]),
    );
  }
}
