import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spacebook/blocs/search/search_bloc.dart';

class SearchBar extends StatelessWidget {
  const SearchBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchCubit, Search>(
      builder: (context, search) => TextField(
        controller: TextEditingController(text: search.query),
        decoration: InputDecoration(
          prefixIcon: const Icon(Icons.search),
          hintText: 'Search',
          suffixIcon: IconButton(
            icon: const Icon(Icons.clear),
            onPressed: () {
              context.read<SearchCubit>().search(null);
            },
          ),
        ),
        onSubmitted: (value) {
          context.read<SearchCubit>().search(value);
        },
      ),
    );
  }
}
