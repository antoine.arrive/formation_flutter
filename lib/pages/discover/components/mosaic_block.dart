import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spacebook/pages/discover/components/mosaic_tile.dart';

import '../../../models/media.dart';

class MosaicBlock extends StatelessWidget {
  final List<Media> medias;
  final bool reverse;

  const MosaicBlock({Key? key, required this.medias, required this.reverse})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 220,
      child: Row(
        textDirection: (reverse) ? TextDirection.rtl : TextDirection.ltr,
        children: [
          AspectRatio(
            aspectRatio: 1.1,
            child: MosaicTile(
              media: medias.elementAt(0),
            ),
          ),
          Expanded(
            child: Column(
              children: [
                // TODO gérer les cas sans éléments
                Expanded(child: MosaicTile(media: medias.elementAt(1))),
                Expanded(child: MosaicTile(media: medias.elementAt(2)))
              ],
            ),
          )
        ],
      ),
    );
  }
}
