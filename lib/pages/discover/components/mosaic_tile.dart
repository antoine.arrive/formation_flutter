import 'package:flutter/cupertino.dart';
import 'package:spacebook/pages/detail/detail_page.dart';

import '../../../models/media.dart';

class MosaicTile extends StatelessWidget {
  final Media media;
  const MosaicTile({Key? key, required this.media}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () =>
            {Navigator.pushNamed(context, DetailPage.name, arguments: media)},
        child: Image.network(
          media.thumbnailUrl,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
