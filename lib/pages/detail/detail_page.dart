import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spacebook/pages/discover/discover_page.dart';
import 'package:spacebook/theme.dart';

import '../../blocs/search/search_bloc.dart';
import '../../models/media.dart';

class DetailPage extends StatelessWidget {
  static const name = '/detail';
  const DetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final media = ModalRoute.of(context)?.settings.arguments as Media;

    return Scaffold(
      appBar: AppBar(
        title: Text(media.title),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Image.network(media.thumbnailUrl),
          ),
          SizedBox(
            height: 52,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: (media.keywords ?? [])
                  .map((e) => Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: GestureDetector(
                          onTap: () {
                            context.read<SearchCubit>().search(e.toString());
                            Navigator.pushNamedAndRemoveUntil(
                                context, DiscoverPage.name, (route) => false);
                          },
                          child: Container(
                              decoration: const BoxDecoration(
                                  color: ThemeApp.kBlue,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                              child: Padding(
                                padding: const EdgeInsets.all(15),
                                child: Text(
                                  e.toString(),
                                  style: const TextStyle(fontSize: 20),
                                ),
                              )),
                        ),
                      ))
                  .toList(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Center(
              child: Text(media.description.toString()),
            ),
          ),
        ],
      ),
    );
  }
}
