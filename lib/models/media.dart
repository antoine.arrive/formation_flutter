class Media{
  final String id;
  final DateTime creation;
  final String title;
  final String? location;
  final String? description;
  final List<String>? keywords;
  final String thumbnailUrl;
  final String collectionUrl;

  Media({required this.id,required this.creation,required this.title, this.location, this.description, this.keywords, required this.thumbnailUrl,required this.collectionUrl });

  Media.fromJson(dynamic data)
      : this(
      id: data['data'][0]['nasa_id'],
      creation: DateTime.parse(data['data'][0]['date_created']),
      title: data['data'][0]['title'],
      description: data['data'][0]['description'],
      location: data['data'][0]['center'],
      keywords: data['data'][0]['keywords']?.cast<String>()?.toList(),
      thumbnailUrl: data['links'][0]['href'],
      collectionUrl: data['href']);
}
