import 'package:dio/dio.dart';
import 'package:spacebook/models/media.dart';

class NasaConnector {
  Dio dio;

  NasaConnector({required this.dio});

  Future<List<Media>> fetchPopular() async {
    Response response =
        await dio.get('https://images-assets.nasa.gov/popular.json');
    Map<String, dynamic> dataFetch = response.data;
    List<dynamic> listMedia = dataFetch['collection']['items'];

    return listMedia.map((m) => Media.fromJson(m)).toList();
  }

  Future<List<Media>> search(String query) async {
    Response response = await dio
        .get('https://images-api.nasa.gov/search?media_type=image&q=$query');
    Map<String, dynamic> dataFetch = response.data;
    List<dynamic> listMedia = dataFetch['collection']['items'];

    return listMedia.map((m) => Media.fromJson(m)).toList();
  }
}
