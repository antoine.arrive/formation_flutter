import 'package:flutter_bloc/flutter_bloc.dart';

import '../../connectors/nasa_connector.dart';
import '../../models/media.dart';

enum SearchStatus {
  initial, // Initial value, no search has been made
  empty, // Search with no result
  pending, // Search query in progress
  success, // Search ended with result
  error, // Search ended with an error
}

class Search {
  String? query;
  List<Media> medias;

  SearchStatus status;

  Search({required this.query, required this.status, required this.medias});
}

class SearchCubit extends Cubit<Search> {
  NasaConnector nasaConnector;

  SearchCubit({required this.nasaConnector})
      : super(Search(query: null, status: SearchStatus.initial, medias: []));

  void search(String? query) async {
    emit(Search(
        query: query, status: SearchStatus.pending, medias: state.medias));

    try {
      List<Media> list = await ((query != null && query.isNotEmpty)
          ? nasaConnector.search(query)
          : nasaConnector.fetchPopular());
      emit(Search(query: query, status: SearchStatus.success, medias: list));
    } catch (e) {
      emit(Search(query: query, status: SearchStatus.error, medias: []));
    }
  }
}
