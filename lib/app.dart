import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spacebook/connectors/nasa_connector.dart';
import 'package:spacebook/pages/detail/detail_page.dart';
import 'package:spacebook/pages/discover/discover_page.dart';
import 'package:spacebook/pages/splash/splash_page.dart';
import 'package:spacebook/theme.dart';

import 'blocs/search/search_bloc.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SearchCubit(nasaConnector: NasaConnector(dio: Dio())),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeApp.spacebookTheme,
        initialRoute: SplashPage.name,
        routes: {
          SplashPage.name: (_) =>
              SplashPage(nasaConnector: NasaConnector(dio: Dio())),
          DiscoverPage.name: (_) => const DiscoverPage(),
          DetailPage.name: (_) => const DetailPage(),
        },
      ),
    );
  }
}
