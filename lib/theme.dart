import 'dart:ui';

import 'package:flutter/material.dart';

class ThemeApp {
  static const Color kDeepBlue = Color.fromRGBO(8, 1, 43, 1);
  static const Color kDarkBlue = Color.fromRGBO(7, 19, 75, 1);
  static const Color kBlue = Color.fromRGBO(6, 59, 147, 1);
  static const Color kLightBlue = Color.fromRGBO(15, 85, 202, 1);

  static final spacebookTheme = ThemeData(
      brightness: Brightness.dark,
      accentColor: Colors.white,
      scaffoldBackgroundColor: kDeepBlue,
      fontFamily: "Montserrat",
      primaryTextTheme: TextTheme(
        headline6: TextStyle(
          fontSize: 14,
          letterSpacing: 1,
        ),
      ),
      appBarTheme: AppBarTheme(
        color: kDeepBlue,
        elevation: 0.0,
      ),
      inputDecorationTheme: const InputDecorationTheme(
          contentPadding: EdgeInsets.zero,
          fillColor: kDarkBlue,
          filled: true,
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(50)),
              borderSide: BorderSide(style: BorderStyle.solid))));
}
